FROM node:14.17.0-alpine as build
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
RUN npm install -g serve
COPY . .
EXPOSE 3000

CMD ["serve", "-s", "build"]